/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package gitlab

import (
	gitlabv1beta1 "gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/apis/gitlab/v1beta1"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
)

const (
	trueString                          = "true"
	falseString                         = "false"
	skipPostDeploymentMigrationsEnvName = "SKIP_POST_DEPLOYMENT_MIGRATIONS"
	migrationTypePost                   = "post"
	migrationTypeLabel                  = "migrationType"
)

func formatMigrationsJob(gitlab *gitlabv1beta1.GitLab, jobTemplate *batchv1.Job, jobName, migrationType string) *batchv1.Job {
	skipPostDeploymentMigrationsEnvValue := trueString

	if migrationType == "post" {
		skipPostDeploymentMigrationsEnvValue = falseString
	}

	if jobTemplate.Spec.Template.ObjectMeta.Labels == nil {
		jobTemplate.Spec.Template.ObjectMeta.Labels = make(map[string]string)
	}

	jobTemplate.Spec.Template.ObjectMeta.Name = jobName
	jobTemplate.Spec.Template.ObjectMeta.Labels[migrationTypeLabel] = migrationType

	if skipPostDeploymentMigrationsEnvValue == "true" {
		for i := range jobTemplate.Spec.Template.Spec.Containers {
			jobTemplate.Spec.Template.Spec.Containers[i].Env = append(jobTemplate.Spec.Template.Spec.Containers[i].Env, v1.EnvVar{Name: skipPostDeploymentMigrationsEnvName, Value: skipPostDeploymentMigrationsEnvValue})
		}
	}

	jobTemplate.ObjectMeta.Name = jobName
	jobTemplate.ObjectMeta.Namespace = gitlab.Namespace

	jobTemplate.Labels = map[string]string{
		gitlabOperatorNameLabelKey: gitlabOperatorNameLabelValue,
		helmLabelSelector:          gitlab.Spec.HelmRelease,
		migrationTypeLabel:         migrationType,
	}

	return jobTemplate
}

func formatSharedSecretsJob(gitlab *gitlabv1beta1.GitLab, jobTemplate *batchv1.Job, jobName string) *batchv1.Job {
	jobTemplate.Spec.Template.ObjectMeta.Name = jobName

	jobTemplate.ObjectMeta.Name = jobName
	jobTemplate.ObjectMeta.Namespace = gitlab.Namespace

	jobTemplate.Labels = map[string]string{
		gitlabOperatorNameLabelKey: gitlabOperatorNameLabelValue,
		helmLabelSelector:          gitlab.Spec.HelmRelease,
	}

	return jobTemplate
}
