/*
Copyright 2018 GitLab.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/apis"
	"gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/controller"
	"gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/version"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/runtime/signals"
)

// Returns the namespace that the operator is bound to it
// If `POD_NAMESPACE` environment variable is set its value
// will be used, otherwise in-cluster namespace of the service
// account will be used.
//
// TODO: Add `--namespace` command line argument
func getNamespace() string {

	// If the environment variable is specified use it
	envVar := os.Getenv("POD_NAMESPACE")
	if len(envVar) > 0 {
		return envVar
	}

	// Try the in-cluster config
	buf, err := ioutil.ReadFile("/var/run/secrets/kubernetes.io/serviceaccount/namespace")
	if err == nil {
		return string(buf)
	}

	// Fall back to all namepsaces. This wont end well!
	return metav1.NamespaceAll
}

// Checks if GITLAB_CLASS environment variable is set
func isGitLabClassMissing() bool {
	return os.Getenv("GITLAB_CLASS") == ""
}

func main() {
	log.Printf(version.GetVersionString())

	if isGitLabClassMissing() {
		log.Fatal("GITLAB_CLASS environment variable is missing.")
	}

	// Get a config to talk to the apiserver
	cfg, err := config.GetConfig()
	if err != nil {
		log.Fatal(err)
	}

	// Create a new Cmd to provide shared dependencies and start components
	namespace := getNamespace()
	log.Printf("Using namespace: %s", namespace)
	mgr, err := manager.New(cfg, manager.Options{
		Namespace: namespace,
	})
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Registering Components.")

	// Setup Scheme for all resources
	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Fatal(err)
	}

	// Setup all Controllers
	if err := controller.AddToManager(mgr); err != nil {
		log.Fatal(err)
	}

	log.Printf("Starting the Cmd.")

	// Start the Cmd
	log.Fatal(mgr.Start(signals.SetupSignalHandler()))
}
