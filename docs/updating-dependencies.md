# Updating Dependencies

### Updating the Kubernetes API client

Rather than updating the go-client directly, we instead specify the version of `sigs.k8s.io/controller-runtime` that
supports the version of the API we want to support.

For example, if want to support Kubernetes 1.9, 1.10, and 1.11, we would target the 1.10 API. Checking https://github.com/kubernetes-sigs/controller-runtime/releases for the last controller runtime version using 1.10.

> **Note:** In this scenario, we picked 1.10 due to the the [Kubernetes Supported Version Skew](https://github.com/kubernetes/community/blob/master/contributors/design-proposals/release/versioning.md#supported-releases-and-component-skew)
> which says that clients must be within +1/-1 minor version of the master node.

Then we would update `Gopkg.toml`, setting the `sigs.k8s.io/controller-runtime` and `sigs.k8s.io/controller-tools` to that version.
Then running the following KubeBuilder command:

```
kubebuilder update vendor
```

See the [kubebuilder documentation](http://kubebuilder.netlify.com/beyond_basics/upgrading_kubebuilder.html) for additional information.
